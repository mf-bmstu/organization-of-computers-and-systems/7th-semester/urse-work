LIBRARY ieee;
USE ieee.std_logic_1164.all; 
LIBRARY work;
configuration Operation_Device_for_stand of TEST_OY is
for bdf_type
	for b2v_inst6 : Operating_Device
		use entity work.Operating_Device(Behavioral);	
		end for;
end for;
end Operation_Device_for_stand;