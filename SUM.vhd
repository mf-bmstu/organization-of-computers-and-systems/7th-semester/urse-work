library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity SUM is
generic (n:integer:=4); -- n параметр, задает разрядность операндов
Port ( 
	a : in  STD_LOGIC_VECTOR (2*n-1 downto 0); -- первое слагаемое
	b : in  STD_LOGIC_VECTOR (2*n-1 downto 0); -- второе слагаемое
	s : out  STD_LOGIC_VECTOR (2*n-1 downto 0) -- сумма
); 
end SUM;

architecture Behavioral of SUM is
begin
	process(a, b)
	variable sum:STD_LOGIC_VECTOR (2*n downto 0);  
	begin
		sum := ('0'&a) + ('0'&b); -- сумируем первое и второе слагаемые с дополнительным разрядом
		if (sum(2*n) = '1') then -- если переполнение
			sum(2*n) :='0'; 
			sum := sum + 1;
		end if;

		s <= sum(2*n-1 downto 0);
	end process;
end Behavioral;