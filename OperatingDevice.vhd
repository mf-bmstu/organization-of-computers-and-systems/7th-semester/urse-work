-- Copyright (C) 2018  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- PROGRAM		"Quartus Prime"
-- VERSION		"Version 18.0.0 Build 614 04/24/2018 SJ Standard Edition"
-- CREATED		"Tue Jan 10 08:06:26 2023"

LIBRARY ieee;
USE ieee.std_logic_1164.all; 

LIBRARY work;

ENTITY OperatingDevice IS 
	PORT
	(
		clk :  IN  STD_LOGIC;
		res :  IN  STD_LOGIC;
		sno :  IN  STD_LOGIC;
		cop :  IN  STD_LOGIC;
		a :  IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
		b :  IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
		sko :  OUT  STD_LOGIC;
		x1 :  OUT  STD_LOGIC;
		x2 :  OUT  STD_LOGIC;
		cs1 :  OUT  STD_LOGIC_VECTOR(7 DOWNTO 0);
		i :  OUT  STD_LOGIC_VECTOR(1 DOWNTO 0);
		pr :  OUT  STD_LOGIC_VECTOR(1 DOWNTO 0);
		ra :  OUT  STD_LOGIC_VECTOR(3 DOWNTO 0);
		rb :  OUT  STD_LOGIC_VECTOR(3 DOWNTO 0);
		rc :  OUT  STD_LOGIC_VECTOR(7 DOWNTO 0);
		y :  OUT  STD_LOGIC_VECTOR(12 DOWNTO 0)
	);
END OperatingDevice;

ARCHITECTURE bdf_type OF OperatingDevice IS 

COMPONENT controldevice
	PORT(reset : IN STD_LOGIC;
		 clock : IN STD_LOGIC;
		 sno : IN STD_LOGIC;
		 cop : IN STD_LOGIC;
		 x1 : IN STD_LOGIC;
		 x2 : IN STD_LOGIC;
		 sko : OUT STD_LOGIC;
		 y : OUT STD_LOGIC_VECTOR(12 DOWNTO 0)
	);
END COMPONENT;

COMPONENT operationsblock
	PORT(clk : IN STD_LOGIC;
		 a : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		 b : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		 y : IN STD_LOGIC_VECTOR(12 DOWNTO 0);
		 x2 : OUT STD_LOGIC;
		 x1 : OUT STD_LOGIC;
		 cs1 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
		 i : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
		 pr : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
		 ra : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
		 rb : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
		 rc : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
	);
END COMPONENT;

SIGNAL	SYNTHESIZED_WIRE_0 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_1 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_2 :  STD_LOGIC_VECTOR(12 DOWNTO 0);


BEGIN 
x1 <= SYNTHESIZED_WIRE_0;
x2 <= SYNTHESIZED_WIRE_1;
y <= SYNTHESIZED_WIRE_2;



b2v_inst : controldevice
PORT MAP(reset => res,
		 clock => clk,
		 sno => sno,
		 cop => cop,
		 x1 => SYNTHESIZED_WIRE_0,
		 x2 => SYNTHESIZED_WIRE_1,
		 sko => sko,
		 y => SYNTHESIZED_WIRE_2);


b2v_inst1 : operationsblock
PORT MAP(clk => clk,
		 a => a,
		 b => b,
		 y => SYNTHESIZED_WIRE_2,
		 x2 => SYNTHESIZED_WIRE_1,
		 x1 => SYNTHESIZED_WIRE_0,
		 cs1 => cs1,
		 i => i,
		 pr => pr,
		 ra => ra,
		 rb => rb,
		 rc => rc);


END bdf_type;