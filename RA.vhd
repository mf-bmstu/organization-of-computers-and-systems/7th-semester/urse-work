library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity RA is
generic(n:integer:=4);
Port ( 
		a : in  STD_LOGIC_VECTOR (n-1 downto 0); -- первый оператор
		y : in  STD_LOGIC_VECTOR (1 downto 0); -- управляющие сигналы для выбора выходных вариантов ra
		ra : out  STD_LOGIC_VECTOR (2*n-1 downto 0); -- выход 
		load : in STD_LOGIC; -- сигнал загрузки первого оператора
		clk : in  STD_LOGIC
);			  
end RA;

architecture Behavioral of RA is
signal state :STD_LOGIC_VECTOR (2*n-1 downto 0); -- регистр для хранения первого оператора
begin
	process(a, clk, y)
	begin
		if clk'event and clk='1' then  
			if load = '1' then 
				state <= a & (n-1 downto 0 => a(n-1));
			end if;
		end if;
		
		if y = "01" then
			ra(2*n-1 downto 0) <= state(2*n-1) & state(2*n-1 downto 1); -- возвращаем сдвинутое на 1 разряд вправо А
		elsif y = "10" then
			ra(2*n-1 downto 0) <= not(state(2*n-1) & state(2*n-1 downto 1)); -- возвращаем инвертированное сдвинутое на 1 разряд вправо А
		elsif y = "11" then
			ra(2*n-1 downto 0) <= (2*n-1 downto n => state(2*n-1)) & state(2*n-1 downto n); -- возвращаем сдвинутое на 4 разряд вправо А
		else
			ra(2*n-1 downto 0) <= (others=>'0');
		end if;
	end process;
end Behavioral;