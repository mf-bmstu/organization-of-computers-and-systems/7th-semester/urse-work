LIBRARY ieee;
USE ieee.std_logic_1164.all; 
USE ieee.numeric_std.all;
USE ieee.std_logic_arith.ALL;
USE ieee.std_logic_unsigned.ALL;
LIBRARY work;

entity Operating_Device is					
generic(n:integer:=4; cnt_n:integer:=2);			
Port (
	a		:	 in STD_LOGIC_VECTOR(n-1 DOWNTO 0);
	b		:	 in STD_LOGIC_VECTOR(n-1 DOWNTO 0);	
	clk	:	 in STD_LOGIC;									
	set	:	 in STD_LOGIC;									
	sno	:	 in STD_LOGIC;									
	sko	:   out STD_LOGIC;											
	rc		:	 out STD_LOGIC_VECTOR(2*n-1 DOWNTO 0);
	cop	:	 in STD_LOGIC := '1'
);
END Operating_Device;

architecture Behavioral of Operating_Device is 

		
component OperationsBlock
generic(n:integer:=n; cnt_n:integer:=cnt_n);										
Port (
	y:  in STD_LOGIC_VECTOR(11 downto 0);			
	x1: out STD_LOGIC;  
	x2: out STD_LOGIC;   		
	a: in STD_LOGIC_VECTOR(n-1 downto 0);    	
	b: in STD_LOGIC_VECTOR(n-1 downto 0);    		
	rc: buffer STD_LOGIC_VECTOR(2*n-1 downto 0);		
	clk: in std_logic		
		
);
end component;

component ControlDevice
Port ( 	
	y: out STD_LOGIC_VECTOR(11 downto 0);    			
	x1: in STD_LOGIC;  
	x2: in STD_LOGIC;
	clock: in STD_LOGIC;                              
	reset: in STD_LOGIC; 										
	sno: in STD_LOGIC;										
	sko: out STD_LOGIC;	
	cop : in STD_LOGIC
);
end component;
		
		
signal X1: STD_LOGIC;  
signal X2: STD_LOGIC;		
signal y_Y: STD_LOGIC_VECTOR(11 downto 0);								
   
begin	
unit_OA: OperationsBlock				
GENERIC MAP(n => n)					
port map (y_Y,X1,X2,a,b,rc,clk);				
unit_YA: ControlDevice			

Port map(y=>y_Y,sko=>sko,x1=>X1,x2=>X2,clock=>clk,reset=>set,sno=>sno,cop=>cop);

end Behavioral;