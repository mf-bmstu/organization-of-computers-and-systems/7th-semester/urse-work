library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity CS2 is
generic(cnt_n:integer:=2);
Port ( 
	cnt : in  STD_LOGIC_VECTOR (cnt_n-1 downto 0); -- значение счётчика
   x1 : out  STD_LOGIC -- условие определяющее равность счётчика нулю
);
end CS2;

architecture Behavioral of CS2 is
begin
	x1 <= '1' when cnt = 0 else '0';
end Behavioral;