library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity RB is
generic(n:integer:=4);
Port ( 
	a : in  STD_LOGIC_VECTOR (n-1 downto 0); -- первый оператор
	b : in  STD_LOGIC_VECTOR (n-1 downto 0); -- второй оператор
	load : in STD_LOGIC; -- сигнал загрузки второго оператора
	clk : in  STD_LOGIC;
	ce : in  STD_LOGIC; -- разрешение тактирования
	x2 : out STD_LOGIC; -- условие определяющее анализируемый разряд B
	rb : out STD_LOGIC_VECTOR (n-1 downto 0)
);
end RB;

architecture Behavioral of RB is
signal tmp1: std_logic_vector(n-1 downto 0) := (others =>'1');
signal tmp2: std_logic_vector(n-1 downto 0) := (others =>'0');
signal state :STD_LOGIC_VECTOR (n-1 downto 0);
begin
	process(a, b)
	begin
		if clk'event and clk='1' then -- по положительному фронту 
			if ce = '1' then -- если есть разрешение тактирования
				if load = '1' then 
					if (a(n-1) = '1' and b = tmp2) then 
						state <= (others => '1');
					elsif ((a = tmp1 and b(n-1) = '1') or (a(n-1) = '1' and b = tmp1)) then
						state <= (others => '0');
					else
						state <= b;
					end if;
				else 
					state <= state(n-1) & state(n-1 downto 1); -- иначе сдвиг вправо с сохранением знака
				end if;
			end if;
		end if;

		x2 <= state(0);
		rb <= state;
	end process;
end Behavioral;

