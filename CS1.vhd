library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity CS1 is
generic(n:integer:=4);
Port ( 
	rr : in  STD_LOGIC_VECTOR (2*n-1 downto 0); -- сохранённое значение суммы
	rc : out  STD_LOGIC_VECTOR (2*n-1 downto 0); -- выходное значение суммы
	clk : in STD_LOGIC;
	y: in STD_LOGIC; -- сигнал на формаирование признака результата
	pr: out STD_LOGIC_VECTOR(1 downto 0) -- признак результата
);
end CS1;

architecture Behavioral of CS1 is
begin
	process(clk, y, rr)
	begin
		if clk'event and clk='1' then -- по положительному фронту 
			if y = '1' then 
				if ((rr = "00000000") or (rr = "11111111")) then
					pr <= "00";
				elsif (rr(2*n-1) = '1') then
					pr <= "10";
				else 
					pr <= "01";
				end if;
			end if;
		end if;
		
		rc <= rr(2*n-2 downto 0) & rr(2*n-1); -- сдвиг входного значения сумма влево для ухода от модифицированного обратного кода
	end process;
end Behavioral;